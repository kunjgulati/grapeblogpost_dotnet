﻿IF NOT EXISTS(Select 1 from Sys.Objects where object_id=OBJECT_ID('Users'))
BEGIN
CREATE TABLE [dbo].[Users] (
    [Id]        INT           IDENTITY (1, 1) NOT NULL,
    [UserName]  VARCHAR (100) NULL,
    [Password]  VARCHAR (100) NULL,
    [FirstName] VARCHAR (100) NULL,
    [LastName]  VARCHAR (100) NULL,
    PRIMARY KEY CLUSTERED ([Id] ASC)
);
END
IF NOT EXISTS(Select 1 from Sys.Objects where object_id=OBJECT_ID('BlogDetails'))
BEGIN
CREATE TABLE [dbo].[BlogDetails] (
    [Id]           INT           IDENTITY (1, 1) NOT NULL,
    [BlogTitle]    VARCHAR (500) NULL,
    [BlogName]     VARCHAR (200) NULL,
    [BlogPostData] VARCHAR (MAX) NULL,
    [Author]       VARCHAR (100) NULL,
    [WhenCreated]  DATETIME      NULL,
    [WhenModified] DATETIME      NULL,
    PRIMARY KEY CLUSTERED ([Id] ASC)
);
END


IF NOT Exists(Select 1 from dbo.Users where UserName = 'TestUser')
BEGIN
INSERT INTO dbo.Users
Values('TestUser','TestPassword','Test','User')
END
