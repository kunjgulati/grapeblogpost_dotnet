﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Blogs_Posts.BlogService;
using Blogs_Posts.Models;

namespace Blogs_Posts.Controllers
{
    [BlogAuthentication]
    public class BlogPostController : ApiController
    {
        //Get -- retrieve Data
        public IHttpActionResult GetAllBlogPosts()
        {
            IList<BlogViewModel> blogs = null;
            try
            {
                using (var x = new BlogsDemoEntities())
                {
                    blogs = x.BlogDetails
                            .Select(b => new BlogViewModel()
                            {
                                Id = b.Id,
                                BlogName = b.BlogName,
                                BlogTitle = b.BlogTitle,
                                BlogPostData = b.BlogPostData,
                                Author = b.Author,
                            //WhenCreated = (DateTime)b.WhenCreated,
                            WhenCreated = b.WhenCreated.Value,
                                WhenModified = b.WhenModified.Value
                            }).ToList();

                    if (blogs.Count == 0)
                        return NotFound();

                    return Ok(blogs);
                }
            }
            catch (Exception e)
            {
                return BadRequest("Something Went Wrong. Please verify the request." + e);
            }

        }

        //Post - Add the data
        public IHttpActionResult PostNewBlogPost(BlogViewModel blog)
        {
            if (!ModelState.IsValid)
                return BadRequest("Invalid Data for Blog Post");

            if (blog.BlogName == null || blog.BlogTitle == null)
                return BadRequest("Blog Attributes not filled.");
            try
            {
                using (var x = new BlogsDemoEntities())
                {
                    x.BlogDetails.Add(new BlogDetail()
                    {
                        BlogName = blog.BlogName,
                        BlogTitle = blog.BlogTitle,
                        BlogPostData = blog.BlogPostData,
                        WhenCreated = DateTime.Now,
                        Author = "KunjGulati"
                    });
                    x.SaveChanges();
                }
                return Ok();
            }
            catch (Exception e)
            {
                return BadRequest("Something Went Wrong. Please verify the request." + e);
            }
        }

        //Put - update data base on id
        public IHttpActionResult PutBlogPost(BlogViewModel blog)
        {
            if (!ModelState.IsValid)
                return BadRequest("Invalid Put Request. Please check");
            try
            {
                using (var x = new BlogsDemoEntities())
                {
                    var checkExistingBlog = x.BlogDetails.Where(c => c.Id == blog.Id)
                        .FirstOrDefault();

                    if (checkExistingBlog != null)
                    {
                        checkExistingBlog.BlogName = blog.BlogName;
                        checkExistingBlog.BlogTitle = blog.BlogTitle;
                        checkExistingBlog.BlogPostData = blog.BlogPostData;
                        checkExistingBlog.WhenModified = DateTime.Now;

                        x.SaveChanges();
                    }
                    else
                        return NotFound();
                }
                return Ok();
            }
            catch (Exception e)
            {
                return BadRequest("Something Went Wrong. Please verify the request." + e);
            }
        }

        //update only the blogPostData
        public IHttpActionResult PatchBlogPost(BlogViewModel blog)
        {
            if (!ModelState.IsValid)
                return BadRequest("Invalid Put Request. Please check");
            try
            {
                using (var x = new BlogsDemoEntities())
                {
                    var checkExistingBlog = x.BlogDetails.Where(c => c.Id == blog.Id)
                        .FirstOrDefault();

                    if (checkExistingBlog != null && blog.BlogPostData != null)
                    {
                        checkExistingBlog.BlogPostData = blog.BlogPostData;
                        checkExistingBlog.WhenModified = DateTime.Now;

                        x.SaveChanges();
                    }
                    else
                        return BadRequest("Invalid Id Or No DataProvided");
                }
                return Ok();
            }
            catch (Exception e)
            {
                return BadRequest("Something Went Wrong. Please verify the request." + e);
            }
        }

        //Delete -- Delete the data based on Id
        public IHttpActionResult DeleteBlogPost(int Id)
        {
            try
            {
                if (Id <= 0)
                    return BadRequest("Blog Id Not Valid");
                using (var x = new BlogsDemoEntities())
                {
                    var blog = x.BlogDetails.Where(b => b.Id == Id).FirstOrDefault();
                    x.Entry(blog).State = System.Data.Entity.EntityState.Deleted;
                    x.SaveChanges();
                }
                return Ok();

            }
            catch (Exception e)
            {
                return BadRequest("Something Went Wrong. Please verify the request." + e);
            }
        }
    }
}
