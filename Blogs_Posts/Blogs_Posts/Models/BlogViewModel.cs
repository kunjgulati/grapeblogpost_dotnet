﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Blogs_Posts.Models
{
    public class BlogViewModel
    {
        public int Id { get; set; }

        public string BlogTitle { get; set; }
        public string BlogName { get; set; }
        public string BlogPostData { get; set; }
        public string Author { get; set; }
        public DateTime? WhenCreated { get; set; }
        public DateTime? WhenModified { get; set; }

    }
}