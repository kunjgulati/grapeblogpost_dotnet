//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Blogs_Posts
{
    using System;
    using System.Collections.Generic;
    
    public partial class BlogDetail
    {
        public int Id { get; set; }
        public string BlogTitle { get; set; }
        public string BlogName { get; set; }
        public string BlogPostData { get; set; }
        public string Author { get; set; }
        public Nullable<System.DateTime> WhenCreated { get; set; }
        public Nullable<System.DateTime> WhenModified { get; set; }
    }
}
