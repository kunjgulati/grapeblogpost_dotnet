﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;


namespace Blogs_Posts.BlogService
{
    public class BlogSecurity
    {
        public static bool Login(string username, string password)
        {
            using (BlogsDemoEntities entities = new BlogsDemoEntities())
            {
                return entities.Users.Any(user => user.UserName.Equals(username,
                    StringComparison.OrdinalIgnoreCase) && user.Password ==password);
            }
        }
    }
}